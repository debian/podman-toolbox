Source: golang-github-containers-toolbox
Section: utils
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders:
 Andrej Shadura <andrewsh@debian.org>,
 Hayley Hughes <hayley@foxes.systems>,
Build-Depends:
 debhelper-compat (= 13),
 apache2-utils <!nocheck>,
 bash-completion,
 bats <!nocheck>,
 go-md2man,
 golang-dbus-dev,
 golang-github-briandowns-spinner-dev,
 golang-github-docker-go-units-dev,
 golang-github-fatih-color-dev,
 golang-github-fsnotify-fsnotify-dev,
 golang-github-harrymichal-go-version-dev,
 golang-github-inconshreveable-mousetrap-dev,
 golang-github-konsorten-go-windows-terminal-sequences-dev,
 golang-github-mattn-go-colorable-dev,
 golang-github-sirupsen-logrus-dev,
 golang-github-spf13-cobra-dev,
 golang-github-spf13-pflag-dev,
 golang-github-stretchr-testify-dev (>= 1.7.0~),
 golang-go,
 golang-golang-x-crypto-dev,
 golang-golang-x-sys-dev,
 libsubid-dev,
 meson,
 podman,
 pkgconf,
 skopeo <!nocheck>,
 shellcheck <!nocheck>,
 systemd-dev,
Standards-Version: 4.6.0
Homepage: https://containertoolbx.org/
Vcs-Browser: https://salsa.debian.org/debian/podman-toolbox
Vcs-Git: https://salsa.debian.org/debian/podman-toolbox.git

Package: podman-toolbox
Architecture: any
Depends:
 flatpak,
 podman,
 uidmap,
 ${misc:Depends},
 ${shlibs:Depends},
 ${toolbx:Depends},
Recommends:
 bash-completion,
Built-Using:
 ${misc:Built-Using},
Provides: toolbx
Description: unprivileged development environment using containers
 Toolbx is a tool which allows the use of containerised command line
 environments. It offers a familiar package based environment for
 developing and debugging software that runs fully unprivileged using Podman.
 .
 A toolbx container is a fully mutable container; when you see yum install
 ansible for example, that's something you can do inside your toolbx
 container, without affecting the base operating system.
 .
 Toolbx used to be known as Container Toolbox.
